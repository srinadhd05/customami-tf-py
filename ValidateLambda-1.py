import os
import boto3
import uuid

from python_terraform import *

BUCKET_NAME = 'validation-phase-1-test'
download_dir = '/tmp/'
SNS_TOPIC = "assesment_complete_trigger"
lambda2_name = "Assesment_Validation"

def lambda_handler(event, context):
    # os.system('cp /var/task/{*.tf,lambda_function.py} /tmp/')
    message = event
    AMI_Name = message['detail']['name']
    s3 = boto3.resource('s3')
    s3Client = boto3.client('s3')
    sns = boto3.client('sns')

    try:
        my_bucket = s3.Bucket(BUCKET_NAME)
        for s3_object in my_bucket.objects.all():
            filename = s3_object.key
            s3Client.download_file(BUCKET_NAME, filename, f'{download_dir}{filename}')
    except ClientError as e:
        return False

    action = os.environ['ACTION']
    region = os.environ['REGION']

    #creating lambda ARN
    acount_id = boto3.client('sts').get_caller_identity().get('Account')
    lambda_arn = ":".join(["arn", "aws", "lambda", region, acount_id, "function", lambda2_name])

    #Extracting SNS ARN
    snsTopic = sns.create_topic(Name=SNS_TOPIC)
    snsTopicArn = snsTopic['TopicArn']

    ami_id = get_ami_id(region, AMI_Name)
    print(ami_id)
    output = execute(region, ami_id)
    template_arn = output['template_arn']['value']
    print("template_arn ===== ", template_arn)
    subscribe_to_event(template_arn, snsTopicArn)
    trigger_lambda(snsTopicArn, lambda_arn, region)
    start_assessment_run(region, template_arn)


def get_ami_id(region, AMI_Name):
    client = boto3.client('ssm', region)

    response = client.get_parameter(
        Name=AMI_Name ,
        WithDecryption=True
    )
    return (response['Parameter']['Value'])


def start_assessment_run(region, arn):
    client = boto3.client('inspector', region)

    response = client.start_assessment_run(
        assessmentTemplateArn=arn,
        assessmentRunName='Gold_AMI_Assessment_Run'
    )


def trigger_lambda(snsTopicArn, lambda_arn, region):
    id = uuid.uuid1()
    sns = boto3.client('sns')
    sns.subscribe(
        TopicArn=snsTopicArn,
        Protocol='lambda',
        Endpoint= lambda_arn
    )

    # To create lambda trigger for SNS
    lambda_client = boto3.client('lambda', region)

    lambda_client.add_permission(
        FunctionName= lambda_arn,
        StatementId=str(id),
        Action='lambda:InvokeFunction',
        Principal='sns.amazonaws.com',
        SourceArn=snsTopicArn
    )


def subscribe_to_event(template_arn, snsTopicArn):
    # client representing
    inspector = boto3.client('inspector')
    sns = boto3.client('sns')

    templatearn = template_arn

    # Subscribing sns with template, event based
    events = [
        'ASSESSMENT_RUN_COMPLETED']  # ,'FINDING_REPORTED','ASSESSMENT_RUN_STATE_CHANGED','ASSESSMENT_RUN_STARTED', ]
    for event in events:
        event_response = inspector.subscribe_to_event(resourceArn=templatearn, event=event, topicArn=snsTopicArn)


def execute(region, ami_id):
    print("In Execution() ---", ami_id)
    tf = Terraform(working_dir="/tmp/", terraform_bin_path='/opt/python/lib/python3.8/site-packages/terraform',
                   variables={"region": region, "AMI_ID": ami_id})
    tf.init()
    approve = {"auto-approve": True}
    tf.apply(capture_output=True, skip_plan=True, **approve)
    stdout = tf.output()
    return stdout

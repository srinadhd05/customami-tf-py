from __future__ import print_function
import boto3
import json
import datetime
import urllib
from botocore.exceptions import ClientError
import time
import uuid
import os

sns = boto3.client('sns')
inspector = boto3.client('inspector')
s3 = boto3.client('s3')
ec2_client = boto3.client('ec2')
ssm_client = boto3.client('ssm')

# SNS topic - will be created if it does not already exist
SNS_TOPIC = "Inspector-Finding-Delivery"

# assesment report name
reportFileName = "assesmentReport.pdf"

# S3 Bucket Name
BUCKET_NAME = "terraform-project-sd"

# Distribution phase lambda name
dist_lambda_name = "custom_AMi"

#SSM parameter pattern name of customizied AMI by build phase
pattern_ami_name = 'ami-latest'

# finding results number
max_results = 250000

# list for no. of High severity incidents
high_severities_list = []

# setting up paginator for the listing findings
paginator = inspector.get_paginator('list_findings')

# filter for searching findings
finding_filter = {'severities': ['High']}


def upload_file(file_name, bucket, object_name=None):
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def lambda_handler(event, context):
    # extract the message that Inspector sent via SNS
    message = event['Records'][0]['Sns']['Message']

    # get inspector notification type
    notificationType = json.loads(message)['event']

    targetArn = json.loads(message)['target']

    region = os.environ['REGION']
    # creating lambda ARN
    acount_id = boto3.client('sts').get_caller_identity().get('Account')
    lambda_arn = ":".join(["arn", "aws", "lambda", region, acount_id, "function", dist_lambda_name])

    # checking for the event type
    if notificationType == "ASSESSMENT_RUN_COMPLETED":

        # getting arn for the assement run
        runArn = json.loads(message)['run']

        # generating the report
        while True:
            reportResponse = inspector.get_assessment_report(
                assessmentRunArn=runArn,
                reportFileFormat='PDF',
                reportType='FULL'
            )
            if reportResponse['status'] == 'COMPLETED':
                break
            time.sleep(5)

        # downloading the report
        file_status = urllib.request.urlretrieve(reportResponse['url'],
                                                 '/tmp/' + reportFileName)

        # uploading to s3
        upload_file('/tmp/' + reportFileName, BUCKET_NAME,
                    runArn.split(":")[5] + "/" + reportFileName)

        # getting the findings for the run Arn
        for findings in paginator.paginate(
                maxResults=max_results,
                assessmentRunArns=[
                    runArn,
                ],
                filter=finding_filter):
            for finding_arn in findings['findingArns']:
                high_severities_list.append(finding_arn)

        # Extracting the Latest AMI id from findings
        findings = inspector.list_findings(assessmentRunArns=[runArn])
        AMI_id = inspector.describe_findings(findingArns=[findings['findingArns'][0]])
        AMI_id = AMI_id['findings'][0]['assetAttributes']['amiId']

        # Creating AMI name unique name
        ami_uq_name = ssm_param_name(AMI_id, pattern_ami_name) + '-' 'approved' + '-' + str(uuid.uuid4())[:12]

        # sending emails if no. of high severity issues are more than 1
        if len(high_severities_list) > 1000:

            subject = "There is High Severity Issue in the assement run"
            messageBody = ("High severity issue is reported in assesment run" + runArn + "\n\n" +
                           "Please check the  detailed report here:" +
                           "s3://" + BUCKET_NAME + "/" + runArn.split(":")[5] + "/" + reportFileName)

            # create SNS topic if necessary
            snsTopicArn = sns.create_topic(Name=SNS_TOPIC)

            # publish notification to topic
            response = sns.publish(
                TopicArn=snsTopicArn['TopicArn'],
                Message=messageBody,
                Subject=subject
            )

            # publish notification to topic
            response = sns.publish(
                TopicArn=snsTopicArn['TopicArn'],
                Message=messageBody,
                Subject=subject
            )

        else:
            trigger_lambda(region, lambda_arn)
            # Creating/Updating the AMI id in parameter store
            ssm_parameter = ssm_client.put_parameter(
                Name=ami_uq_name,
                Value=str(AMI_id),
                Description='Cutom_AMI_ID',
                Type='String',
                Overwrite=True
            )

            # Updating the security APPROVED tag
            ssm_tags = ssm_client.add_tags_to_resource(
                ResourceType='Parameter',
                ResourceId=ami_uq_name,
                Tags=[
                    {
                        'Key': 'status',
                        'Value': 'Security_Approved'
                    },
                ]
            )

        # Extracting Instance details
        instance_details = ec2_client.describe_instances(Filters=[
            {
                'Name': 'image-id',
                'Values': [AMI_id]
            }]
        )

        # Extracting Instance id and security group details
        for reservation in instance_details['Reservations']:
            for instance in reservation['Instances']:
                if instance['State']['Name'] == 'running':
                    for securityGroup in instance['SecurityGroups']:
                        sg_name = securityGroup['GroupName']
                        instance_id = instance['InstanceId']
        print(instance_id)
        print(sg_name)
        # Terminating instance
        ec2_client.terminate_instances(
            InstanceIds=[instance_id]
        )

        # Destroying assessment
        inspector.delete_assessment_target(
            assessmentTargetArn=targetArn
        )

        time.sleep(120)
        # Deleting Security group
        ec2_client.delete_security_group(GroupName=sg_name)


def trigger_lambda(region, lambda_arn):
    id = uuid.uuid1()
    client = boto3.client('events', region)
    rule_name = 'ssm_update_event'
    rule_res = client.put_rule(Name=rule_name,
                               EventPattern='''
                                 {
                                    "source": [
                                        "aws.ssm"
                                    ],
                                    "detail-type": [
                                        "Parameter Store Change"
                                    ],
                                    "detail": {
                                        "name": [{"prefix": "ami-latest-approved"}],
                                        "operation": [
                                            "Create",
                                            "Update"
                                        ]
                                    }
                                   }
                                   ''',
                               State='ENABLED',
                               Description="Find the event changes for SSM")

    print("res ==== ", rule_res)

    lambda_client = boto3.client('lambda', region)

    lambda_client.add_permission(
        FunctionName=lambda_arn,
        StatementId=str(id),
        Action='lambda:InvokeFunction',
        Principal='events.amazonaws.com',
        SourceArn=rule_res['RuleArn']
    )

    response = client.put_targets(Rule='ssm_update_event', Targets=[{"Arn": lambda_arn, "Id": '1'}])
    print("\nresponse ==== ", response)

def ssm_param_name(AMI_id, pattern_ami_name):
    ami_names = []
    param_names = ssm_client.describe_parameters(
        Filters=[
            {
                'Key': 'Name',
                'Values': [pattern_ami_name,]
            },
        ]
    )
    for parameter in param_names['Parameters']:
            ami_names.append(parameter['Name'])
    for AmiName in ami_names:
        ssm_ami = ssm_client.get_parameter(Name=AmiName)
        if ssm_ami['Parameter']['Value'] == AMI_id:
            ami_name = AmiName
            break

    return ami_name